package com.cognizant.moviecruiser.dao;

import java.util.ArrayList;

import java.util.List;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;



import com.cognizant.moviecruiser.model.Movie;

@Service
public class MovieDao 
{
	
        private final static Logger LOGGER = LoggerFactory.getLogger(MovieDao.class);
	
        public static List<Movie> Movie_list = new ArrayList<Movie>();
	
	
        @Autowired
	MovieDao moviedao;
	

	public MovieDao()
        {

		LOGGER.info("Start");

		ApplicationContext context = new ClassPathXmlApplicationContext("movie.xml");

		Movie_list = context.getBean("movieList", java.util.ArrayList.class);
		LOGGER.debug("Movie : {}", Movie_list.toString());
		LOGGER.info("End");
	}
	
	
	public List<Movie> getMovieListAdmin()
        {
		
		return Movie_list;
	}

	
	public List<Movie> getMovieListCustomer()
        {
		
		return Movie_list;
	}

       
        public void toEditMovie(int ID, Movie movie)
        {
		
        Movie Res=findById(ID);
		
        ID=ID-1;

        if(Res.equals(null))
        {
	   
			throw new EmptyFoundException();

	       }
	      else
	      {

	    	  Movie_list.get(ID).setTitle(movie.getTitle());
	    	  Movie_list.get(ID).setActive(movie.isActive());
	    	  Movie_list.get(ID).setBoxOffice(movie.getBoxOffice());
	    	  Movie_list.get(ID).setDateOfLaunch(movie.getDateOfLaunch());
	    	  Movie_list.get(ID).setGenre(movie.getGenre());
	    	  Movie_list.get(ID).setHasTeaser(movie.isHasTeaser());
		

	       }

	  return;
		
	}

	public Movie findById(int mv_iD) {
		Movie mm=Movie_list.get(mv_iD);
		return mm;
	}

}
