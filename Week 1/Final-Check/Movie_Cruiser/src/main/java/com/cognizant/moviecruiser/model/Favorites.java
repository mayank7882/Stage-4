package com.cognizant.moviecruiser.model;

import java.util.List;





public class Favorites {
	
	private int ID;
	
	private int us_ID;
	
	
	private int mv_ID;

	public Favorites() {

		super();
	}

	public Favorites(int us_ID, int mv_ID) {
		super();
		this.us_ID = us_ID;
		this.mv_ID = mv_ID;
	}

	public Favorites(int ID, int us_ID, int mv_ID) {
		super();
		this.ID = ID;
		this.us_ID = us_ID;
		this.mv_ID = mv_ID;
	}

	public int getId() {
		return ID;
	}

	public void setId(int ID) {
		this.ID = ID;
	}

	public int getUs_id() {
		return us_ID;
	}

	public void setUs_id(int us_ID) {
		this.us_ID = us_ID;
	}

	public int getMv_id() {
		return mv_ID;
	}

	@Override
	public String toString() {
		return "Favorites [ID=" + ID + ", us_ID=" + us_ID + ", mv_ID=" + mv_ID + "]";
	}

	public void setMv_id(int mv_ID) {
		this.mv_ID = mv_ID;
	}

	

}
